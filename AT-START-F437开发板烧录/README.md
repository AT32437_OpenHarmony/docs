1、AT-START-F437开发板自带块ATlink调试接口。首先安装[ATlink驱动](./Artery_ICP_Programmer_V3.0.18.zip)

![alt text](./pic/image.png)

2.打开ICP烧录工具

![alt text](./pic/image-1.png)

3.点击连接，添加bin文件或者hex文件。开始下载文件到开发板上
![alt text](./pic/image-2.png)

3.最后完成烧录`断开连接`
![alt text](./pic/image-3.png)