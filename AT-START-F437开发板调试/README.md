1.需要准备的工具

usb转ttl模块

![alt text](image.png)

2.将usb转ttl模块的rx接在开发板PA9引脚上

![alt text](image-1.png)

3.打开串口工具，使用usb转ttl模块将开发板和电脑起来，波特率设置为115200

![alt text](image-2.png)