## 1.AT-START-F437雅特力官方开发板相关资料

> 移植基于at32f437雅特力官方开发板AT-START-F437

[AT-START-F437雅特力官方开发板相关资料](./AT-START-F437雅特力官方开发板相关资料/README.md)


![alt text](./media/image3.png)

## 2.AT32F437芯片相关资料

[AT32F437芯片相关资料](./AT32F437芯片相关资料/README.md)

## 3.如何在AT32F437ZMT上使用OpenHarmony轻量系统开发


1.首先搭建好OpenHarmony开发环境后，下载OpenHarmony轻量系统4.1r的hi3861代码

```
# 下载4.1 r版本hi3861源码
repo init -u https://gitee.com/openharmony/manifest -b refs/tags/OpenHarmony-v4.1-Release -m chipsets/hispark_pegasus.xml -g ohos:mini
repo sync -c
repo forall -c 'git lfs pull'

# 安装依赖
./build/build_scripts/env_setup.sh
source ~/.bashrc

# 下载编译工具
bash build/prebuilts_download.sh

# 执行一遍3861的编译命令确认环境有没有问题
./build.sh --product-name wifiiot_hispark_pegasus --ccache --no-prebuilt-sdk
```

2.将本组织下AT32F437的[vendor](https://gitee.com/AT32437_OpenHarmony/vendor)和[device](https://gitee.com/AT32437_OpenHarmony/device)替换掉上一步拉取的3861代码的vendor和device目录

```
rm -rf device
git clone https://gitee.com/AT32437_OpenHarmony/device.git device
rm -rf vendor
git clone https://gitee.com/AT32437_OpenHarmony/vendor.git vendor
```

![alt text](./media/image4.png)

3.编译固件
```
./build.sh --product-name AT-START-F437 --ccache --no-prebuilt-sdk
```

![alt text](./media/image2.png)

## 4.AT-START-F437雅特力官方开发板如何烧录程序

[AT-START-F437雅特力官方开发板烧录程序](./AT-START-F437开发板烧录/README.md)

## 5.AT-START-F437雅特力官方开发板如何调试

[AT-START-F437雅特力官方开发板烧录调试](./AT-START-F437开发板调试/README.md)

